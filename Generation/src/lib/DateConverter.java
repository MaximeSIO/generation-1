package lib;

import java.util.Calendar;
import java.util.GregorianCalendar;

public abstract class DateConverter {

	public static String dateToString(GregorianCalendar date)	{
		String dateFormat;
		dateFormat = String.valueOf(date.get(Calendar.DATE))+"-"+String.valueOf(date.get(Calendar.MONTH))+"-"+String.valueOf(date.get(Calendar.YEAR));
		return dateFormat;
	}
	
	public static String hourToString(GregorianCalendar date)	{
		String dateFormat;
		String h = String.valueOf(date.get(Calendar.HOUR_OF_DAY));
		if(date.get(Calendar.HOUR_OF_DAY)<10)
			h="0"+String.valueOf(date.get(Calendar.HOUR_OF_DAY));
		String m = String.valueOf(date.get(Calendar.MINUTE));
		if(date.get(Calendar.MINUTE)<10)
			m="0"+String.valueOf(date.get(Calendar.MINUTE));
		dateFormat = h+":"+m;
		return dateFormat;
	}
	
	public static GregorianCalendar stringToDateHour(String strDate) {
//			Permet de convertir un String au format JJ-MM-AAAA#HH:MM en un GregorianCalendar
		String[] detailsDate, detailsHour, details;
		int year, month, day, hour, minute;
		details = strDate.split("#");
		detailsDate = details[0].split("-");
		detailsHour = details[1].split(":");
		year=Integer.parseInt(detailsDate[2]);
		month=Integer.parseInt(detailsDate[1]);
		day=Integer.parseInt(detailsDate[0]);
		hour=Integer.parseInt(detailsHour[0]);
		minute=Integer.parseInt(detailsHour[1]);
		return new GregorianCalendar(year,month,day,hour,minute);
	}
	
	public static GregorianCalendar stringToDate(String strDate) {
		// Permet de convertir un String au format JJ-MM-AAAA en un GregorianCalendar
		String[] detailsDate;
		int year, month, day;
		detailsDate = strDate.split("-");
		year=Integer.parseInt(detailsDate[2]);
		month=Integer.parseInt(detailsDate[1]);
		day=Integer.parseInt(detailsDate[0]);
		return new GregorianCalendar(year,month,day);
	}
}
